﻿using KursovaWPF.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KursovaWPF.DataContext
{
    class ArrayDBContext : DbContext
    {
        public ArrayDBContext() { }
        public ArrayDBContext (DbContextOptions options) : base(options) { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=C:\\Users\\1\\Desktop\\C#\\KursovaWPF\\ArraysDB.db");
        }
        public DbSet<ArrayDBModel> Arrays { get; set; }
    }
}
