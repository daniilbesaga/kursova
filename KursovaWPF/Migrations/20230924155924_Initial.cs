﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KursovaWPF.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Arrays",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Array = table.Column<string>(type: "TEXT", nullable: false),
                    BubbleSortTime = table.Column<double>(type: "REAL", nullable: false),
                    SelectionSortTime = table.Column<double>(type: "REAL", nullable: false),
                    InsertsSortTime = table.Column<double>(type: "REAL", nullable: false),
                    QuickSortTime = table.Column<double>(type: "REAL", nullable: false),
                    MergeSortTime = table.Column<double>(type: "REAL", nullable: false),
                    ShellSortTime = table.Column<double>(type: "REAL", nullable: false),
                    HeapSortTime = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Arrays", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Arrays");
        }
    }
}
