﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KursovaWPF.Models
{
    public class ArrayDBModel
    {
        public int Id { get; set; }
        public string Array { get; set; }
        public double BubbleSortTime { get; set; }
        public double SelectionSortTime { get; set; }
        public double InsertsSortTime { get; set; }
        public double QuickSortTime { get; set; }
        public double MergeSortTime { get; set; }
        public double ShellSortTime { get; set; }
        public double HeapSortTime { get; set; }
    }
}
